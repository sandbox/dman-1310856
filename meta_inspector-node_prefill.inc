<?php
/**
 * @file
 * Routines to try and work around node form validation so that we can pre-fill
 * required values with data from the uploaded file.
 *
 * This got insane working against the FAPI, so I pulled the functions out here to concentrate on the real job elsewhere.
 */

/**
 * Implementation of hook_form_alter()
 *
 * Adjust the #required title field to instead allow us to fill it in
 * with data during submission.
 */
function meta_inspector_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form['#node']) && isset($form['#method']) && $form['#node']->type . '_node_form' == $form_id) {
    // A node form
    $node = $form['#node'];

    // IF this node type
    // CAN potentially be populated by data provided by a file upload
    // THEN we should force a preview to allow this data to be extracted
    // AND not block validation due to complaints by required fields yet.

    // I wanted to fix the UI to let it happen all-in-one .. but can't find a way through the race conditions of
    // file-upload vs required field validation.
    // I can't scan the file until after the form is processed, and
    // By the time I find that data is missing, it's too late to block the form submit.

    // Depending on the mapping rules, some of the otherwise #required
    // fields may be able to be auto-populated.
//  This means we need to make them not-required so form submission can progress far enough for that to happen.
    // node_auto_title does something similar, so I guess it's safe

    // First, check if we have an autopopulate source at all...
    $scan_rules = meta_inspector_scan_rules($node->type);
    if (!empty($scan_rules['populate'])) {
      // If any of the scanned fields may populate the node itself, loosen the validation.
      $mapping_rules = variable_get('meta_mapping_rules', meta_inspector_default_mapping_rules());
      // recursively hit the form and see if the rules need to be changed
      $disabled_some_validation = meta_inspector_soften_validation($form, $mapping_rules);
    }
    if ($disabled_some_validation) {
      #$form['buttons']['submit']['#disabled'] = TRUE;
      #$form['buttons']['submit']['#attributes']['title'] = "Some validation has been disabled until metadata is available";
    }
    dpm(get_defined_vars());
  }
}



/**
 * Recurse through a form, and reduce/replace validation requirements for specified fields.
 *
 * If anything in the $mapping array is #required - replace that validation with our own version.
 *
 * @return whether any validation was disabled. If so, the parent form should not be allowed to be submitted as it may be incomplete.
 */
function meta_inspector_soften_validation(&$form, $mapping_rules) {
  $disabled_some_validation = FALSE;
  foreach (element_children($form) as $field_id) {
    $field = $form[$field_id];
    if (in_array($field_id, $mapping_rules) || ($field_id == 'body' && in_array('body_field', $mapping_rules))) {
      // Frankie says relax
      $form[$field_id]['#element_validate'][] = 'meta_inspector_prefill_element';
      if (!empty($form[$field_id]['#required'])) {
        $form[$field_id]['#required'] = FALSE;
        $form[$field_id]['#element_validate'][] = 'meta_inspector_validate_required_element';
        $disabled_some_validation = TRUE;
      }
      $form[$field_id]['#description'] = t('If possible, data from the attached file will be used to set this value when you "Preview" - unless you set it yourself.');
    }
    // recurse
    $disabled_some_validation |= meta_inspector_soften_validation($form[$field_id], $mapping_rules);
  }
  return $disabled_some_validation;
}

/**
 * During the validation phase, insert data that we may have found
 *
 */
function meta_inspector_prefill_element(&$element, &$form_state) {
  // ensure that any added file has been expanded to fill in the required data fields it maps too
  meta_inspector_autofill_form($form_state);

  if ((!count($element['#value']) || (is_string($element['#value']) && strlen(trim($element['#value'])) == 0))) {
    // See if we can pre-fill it from extracted data - I may be operating on any value, but usually just the title
    if (!empty($form_state['prefilled'][$element['#name']])) {
      $found_value = $form_state['prefilled'][$element['#name']];
      form_set_value($element, $found_value, $form_state);
      // Also need to set it on the element directly so meta_inspector_validate_required_element knows it's been added.
      $element['#value'] = $found_value;
      watchdog(__FUNCTION__, "Pre-filled %element_name with a value extracted from the file metadata", array('%element_name' => $element['#name']), WATCHDOG_DEBUG);
    }
  }
  #dpm(get_defined_vars());
}

/**
 * Callback to replace core #required validation.
 * Scary thought, but I need to turn off validation for long enough for the
 * auto-filled values - eg for $node->title - to get populated
 *
 * node_auto_title does something similar, so I guess it's safe
 *
 * This is called AFTER meta_inspector_prefill_element(), so that it only complains if the prefil step failed.
 *
 * Pulled from form.inc
 */
function meta_inspector_validate_required_element($element, &$form_state) {
  // Do not block the form with validation on preview
  // Otherwise we cannot progress to actually scanning the uploaded data.
  if ($form_state['values']['op'] == t('Preview')) {
    return;
  }
  if ((!count($element['#value']) || (is_string($element['#value']) && strlen(trim($element['#value'])) == 0))) {
    form_error($element, t('%name field is required (and I failed to guess it from metadata inspection).', array('%name' => $element['#title'])));
  }
}

/**
 * Get meta data from the attached file field, so we can set title & body etc in the submitted form values if appropriate.
 *
 * Scan the submitted form data, make a note of other fields based on what was submitted.
 *
 * Interim data will be stored in $form_state['storage']['prefilled']
 *
 * In this function, data is scanned but not actually added to the form yet.
 * @see meta_inspector_prefill_element for that.
 *
 * @param array $form_state
 */
function meta_inspector_autofill_form(&$form_state) {
  if (isset($form_state['prefilled'])) {
    return $form_state['prefilled'];
  }
  // The submitted data gives us a clue about what to look for.
  $node_array = $form_state['values'];

  // This provides a description of where to extract data that could go into the node
  $scan_rules = meta_inspector_scan_rules($node_array['type']);
  if (!empty($scan_rules['populate'])) {
    $field_id = $scan_rules['populate'];
    // Data from this file should be copied into the node
    // eg a node_gallery_image->field_node_gallery_image
    watchdog(__FUNCTION__, 'meta inspector rules indicate we should copy data from %field_id into this %node_type node', array('%field_id' => $field_id, '%node_type' => $node->type), WATCHDOG_INFO);
    $file_field = reset($node_array[$field_id]);
    if (!empty($file_field['filepath'])) {
      $meta = module_invoke_all('metadata_from_file', $file_field['filepath']);

      // Dummy node, Only used as a clone to put some values onto before inspecting it:
      $data_node = (object) $form_state['values'];
      meta_inspector_merge_metadata_with_node($data_node, $meta);
      $form_state['prefilled'] = (array) $data_node;
    } // file exists
    else {
      dpm('No field to scan yet!');
      dpm(get_defined_vars());
    }
  } // field data should get copied to node later
}


/**
 * An extra function called towards the end of validation.
 *
 * Try and fill in the required fields if we can (Could not do it with
 * element_validate as that was called before the file was uploaded and
 * processed.)
 */
function meta_inspector_fill_in_values(&$form, &$form_state) {
  $title_element = $form['title'];
  $title = $form_state['values']['title'];

  // image_form_validate() loads and prepares the image earlier
  // so the file should be available for inspection

  $node_array = $form_state['values'];
  if (empty($title) && !empty($node_array['images'][IMAGE_ORIGINAL]) ) {
    // Loading this metadata twice may be inefficient. TODO cache?
    $meta = module_invoke_all('metadata_from_file', $node_array['images'][IMAGE_ORIGINAL]);

    // Dummy node, Only used as a clone to put some values onto before inspecting it:
    $node = (object) $form_state['values'];
    meta_inspector_merge_metadata_with_node($node, $meta, FALSE);

    // Found it! (maybe)
    if ($title = $node->title) {
      form_set_value($title_element, $title, $form_state);
    }
  }

  // If that didn't work, then restore the 'required' behaviour
  if (empty($title)) {
    form_error($title_element, t('%name field is required.', array('%name' => $title_element['#title'])));
  }
}
