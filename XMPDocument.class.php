<?php

/**
 * Represents an XMP document (internally a DOM document)
 *
 * Generally, you would create an object, then assign a set of properties to it.
 * These properties should be either full URIs or PNames (eg "dc:title")
 *
 * addData($data_array) can be used for mass creation.
 *
 * When a XMPDocument is created, it is given a fresh xap:MetadataDate.
 * Submitted xap:MetadataDate values are ignored.
 *
 */
class XMPDocument {
  public $uri;
  private $document;
  private $xmpmeta;
  private $rdf;
  private $xmpns = 'adobe:ns:meta/';
  private $rdfns = 'http://www.w3.org/1999/02/22-rdf-syntax-ns#';
  private $NamespacedChunk = array();

  private $ns_count = 0;

  /**
   * A running collection of just what namespaces are actually in use
   */
  private $used_ns = array ();
  /**
   * A list of known namespace shortnames
   */
  private $ns = array ();

  /**
   * A collections of statements, grouped by namespace
   *
   * To  emulate the Adobe way, statements about things in different
   * namespaces will be stored in sligtly different groups
   * Even though RDF parsers treat them all as one.
   */
  private $descriptionGroups = array ();

  /**
   * Initialize some values
   */
  function __construct() {
    $this->ns = array(
      'dc' => "http://purl.org/dc/elements/1.1/",
      'tiff' => "http://ns.adobe.com/tiff/1.0/",
      'xap' => "http://ns.adobe.com/xap/1.0/",
      'xapMM' => "http://ns.adobe.com/xap/1.0/mm/",
      'exif' => "http://ns.adobe.com/exif/1.0/",
      'crs' => "http://ns.adobe.com/camera-raw-settings/1.0/",
      'photoshop' => "http://ns.adobe.com/photoshop/1.0/",
      'MicrosoftPhoto' => "http://ns.microsoft.com/photo/1.0/",
      # Beware, Bridge may produce MicrosoftPhoto URI with no slash!
      'Iptc4xmpCore' => "http://iptc.org/std/Iptc4xmpCore/1.0/xmlns/",
      'lr' => "http://ns.adobe.com/lightroom/1.0/",
    );
    $this->used_ns = array (
      $this->rdfns,
    );

    $this->document = new DOMDocument('1.0', 'UTF-8');
    $this->document->formatOutput = TRUE;
    $this->xmpmeta = $this->document->createElementNS($this->xmpns, 'x:xmpmeta', ' ');
    $this->xmpmeta->setAttribute('xmlns:x', 'adobe:ns:meta/');
    $this->xmpmeta->setAttributeNS($this->xmpns, 'x:xmptk', basename(__FILE__));
    $this->document->appendChild($this->xmpmeta);
    $this->rdf = $this->document->createElementNS($this->rdfns, 'rdf:RDF', ' ');
    $this->xmpmeta->appendChild($this->rdf);

    // As we are constructing this block of metadata,
    // update the xap:MetadataDate FTR
    $this->addProperty('xap:MetadataDate', date('c'));
  }

  /**
   * Populate the resource with data from a structured array
   */
  public function addData($data) {

    // In freaky cases - when combining sources of metadata -
    // You may find different aliases that need to add values to the same place.
    // eg source may contain:
    //
    // $data['dc:subject'] = 'aircraft';
    // $data['http://purl.org/dc/elements/1.1/subject'] = 'airshow';
    //
    // RDF will correctly recognize these as being the same group.
    // But XMP parser (Bridge)
    // will complain if I don't merge them myself first.

    // Merge pname aliases before adding the properties
    // This ensures the grouping does not confuse Bridge
    foreach ($data as $varname => $vals) {
      $pname = $this->getPname($varname);
      if (empty($pname)) {
        // invalid name, discard
        unset($data[$varname]);
        continue;
      }
      if ($varname != $pname) {
        // Migrate the vales over to the pname version
        $data[$pname] = array_merge((array)@$data[$pname], (array)$vals);
        // As we may be merging from different sources which say pretty much the same thing in slightly different ways,
        // we should also reduce duplicates, or else every time we process the keyword list may double.
        unset($data[$varname]);
      }
    }
    // At this point, everything is in cannonic pnames

    foreach ($data as $varname => $vals) {
      // xtra-special case. this is not carried through
      if ($varname == 'xap:MetadataDate') continue;

      // The data may contain nested arrays, but addProperty is clever enough.
      $this->addProperty($varname, $vals);
    }
  }

  /**
   * Set a single, namespaced value on the resource
   *
   * Also handles array content as best it can, placing simple indexed values
   * into an rdf:Bag, and setting values keyed beginning with @ as attributes on
   * the element it's creating.
   */
  public function addProperty($name, $content) {

    if (is_array($content)) {
      // Remove duplicates. This will corrupt the rdf:Seq=tiff:BitsPerSample
      // But the rest of the time do what is expected and best;
      $content = array_unique($content); // does retain keys

      // Removed duplicates first thing,
      // because now we can ID arrays that are actually singletons.
      if (count($content) == 1) {
        $content = reset($content);
      }
    }

    // Check its name - a pname or a uri?
    $pname = $this->getPname($name);
    $ns = $this->getPnameNamespace($pname);
    $prefix = $this->getPrefix($ns);

    if (empty($pname) || empty($ns) || empty($prefix)) {
      trigger_error("Invalid property name. Not making an element [$name => $content] xmlns:$prefix = '$ns'", E_USER_NOTICE);
      return;
    }

    // Does a namespaced container already exist for this?
    if (! isset($this->NamespacedChunk[$ns])) {
      #dpm("creating prefix resource block $prefix");
      $container = $this->addNamespacedChunk($prefix, $ns);
    }
    else {
      $container = $this->NamespacedChunk[$ns];
    }

    $element = $this->document->createElement($pname);
    $container->appendChild($element);

    if (! is_array($content)) {
      // Just set content string!
      if (is_string($content)) {
        $element->appendChild($this->document->createTextNode($content));
      }
      else {
        drupal_set_message("Content of $pname was not a string. Not adding it to XMP", 'warning');
      }
      return $element;
    }
    else {
      // Handle arrays.
      // Arrays may indicate nested sets (rdf:Bag) or attributes
      // or even concievably a combination of the two.


      foreach($content as $item_name => $content_item) {
        // Handle attributes different from structs
        // If the key begins with '@'
        if (preg_match('/^@(.*)/', $item_name, $matches)) {
          // It's an attribute
          $item_name = $matches[1];

          // It's almost certain that the ns for the atts found here will be identical to then above one, but check anyway.
          $item_pname = $this->getPname($item_name);
          $item_ns = $this->getPnameNamespace($item_pname);
          $item_prefix = $this->getPrefix($item_ns);

          $element->setAttributeNS($item_ns, $item_pname, $content_item);
        }
        else {
          // Not an att, a child item.
          // Place them all in an rdf:Bag.
          // XMP is anal about collections, but doesn't seem to mind
          // if I use a Bag where it expects a Seq or an Alt.
          // $item_name index is not used for these values.
          if (empty($bag)) {
            // Make a bag on the fly if not already created
            $bag = $this->document->createElementNS($this->rdfns, 'rdf:Bag');
            $element->appendChild($bag);
          }
          $li = $this->document->createElementNS($this->rdfns, 'rdf:li', $content_item);
          $bag->appendChild($li);
        }
      }
    }
    return $element;
  }

  /**
   * Add an rdf:Description node just to hold one namespaced set of data.
   *
   * This emulates the Adobe serialization which creates multiple rdf:
   * Description blocks, each for different xml namespaces.
   */
  public function addNamespacedChunk($prefix, $namespace) {
    $element = $this->document->createElementNS($this->rdfns, 'rdf:Description');
    $element->setAttribute('xmlns:'.$prefix, $namespace);
    $element->setAttributeNS($this->rdfns, 'rdf:about', $this->uri);
    $this->rdf->appendChild($element);
    $this->NamespacedChunk[$namespace] = $element;
    return $element;
  }

  public function createElementNS($ns, $nodeName, $nodeValue = null) {
    return $this->document->createElementNS($ns, $nodeName, $nodeValue);
  }

  /**
   * Returns the text/xml rendering of this document
   */
  public function getXML() {
    return $this->document->saveXML();
  }

  /**
   * Below functions stolen from ARC2 library
   *
   * Utilities to resolve shortname to longname and back
   */

  /**
   * Given a URI, convert it to shorthand, re-using known namespaces if possible.
   */
  function getPName($v, $connector = ':') {
    /* is already a pname */
    if ($ns = $this->getPNameNamespace($v)) {
      if (!in_array($ns, $this->used_ns))
        $this->used_ns[] = $ns;
      return $v;
    }
    /* new pname */
    if ($parts = $this->splitURI($v)) {
      if (empty($parts[1])) {
        #trigger_error("Failed to make a pname from '$v'", E_USER_WARNING);
        return NULL;
      }
      /* known prefix */
      foreach ($this->ns as $prefix => $ns) {
        if ($parts[0] == $ns) {
          if (!in_array($ns, $this->used_ns))
            $this->used_ns[] = $ns;
          return $prefix . $connector . $parts[1];
        }
      }
      /* new prefix */
      $prefix = $this->getPrefix($parts[0]);
      return $prefix . $connector . $parts[1];
    }
    return $v;
  }

  function getPNameNamespace($v) {
    if (!preg_match('/^([a-z0-9\_\-]+)\:([a-z0-9\_\-\.\%]*)$/i', $v, $m))
      return 0;
    if (!isset ($this->ns[$m[1]]))
      return 0;
    return $this->ns[$m[1]];
  }

  /**
   * Given a full URI, return the short prefix it's aliased to.
   */
  function getPrefix($ns) {
    $nsp = array_flip($this->ns);
    if (!isset ($nsp[$ns])) {
      $prefix = 'ns' . $this->ns_count;
      $this->ns[$prefix] = $ns;
      $nsp[$ns] = $prefix;
      $this->ns_count++;
    }
    if (!in_array($ns, $this->used_ns)) {
      $this->used_ns[] = $ns;
    }
    return $nsp[$ns];
  }

  function expandPName($v, $connector = ':') {
    $re = '/^([a-z0-9\_\-]+)\:([a-z0-9\_\-]+)$/i';
    if ($connector == '-') {
      $re = '/^([a-z0-9\_]+)\-([a-z0-9\_\-]+)$/i';
    }
    if (preg_match($re, $v, $m) && isset ($this->ns[$m[1]])) {
      return $this->ns[$m[1]] . $m[2];
    }
    return $v;
  }

  /**
   * Given a full URI, guess where the cutoff is, returning a base URI which can
   * be aliased as a namespace, and a leaf string identifying the ID within that
   * namespace.
   */
  function splitURI($v) {
    /* the following namespaces may lead to conflated URIs,
     * we have to set the split position manually
    */
    if (strpos($v, 'www.w3.org')) {
      $specials = array (
        'http://www.w3.org/XML/1998/namespace',
        'http://www.w3.org/2005/Atom',
        'http://www.w3.org/1999/xhtml',
        'http://ns.microsoft.com/photo/1.0',

      );
      foreach ($specials as $ns) {
        if (strpos($v, $ns) === 0) {
          $local_part = substr($v, strlen($ns));
          if (!preg_match('/^[\/\#]/', $local_part)) {
            return array (
              $ns,
              $local_part
            );
          }
        }
      }
    }
    /* auto-splitting on / or # */
    //$re = '^(.*?)([A-Z_a-z][-A-Z_a-z0-9.]*)$';
    if (preg_match('/^(.*[\/\#])([^\/\#]+)$/', $v, $m))
      return array (
        $m[1],
        $m[2]
      );
    /* auto-splitting on last special char, e.g. urn:foo:bar */
    if (preg_match('/^(.*[\:\/])([^\:\/]+)$/', $v, $m))
      return array (
        $m[1],
        $m[2]
      );
    return array (
      $v,
      ''
    );
  }

}
?>