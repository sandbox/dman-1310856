api = 2
core = 6.x

; Libraries
libraries[pjmt][download][type] = "get"
libraries[pjmt][download][url] = "http://www.ozhiker.com/electronics/pjmt/PHP_JPEG_Metadata_Toolkit_1.12.zip"
libraries[pjmt][directory_name] = "PHP_JPEG_Metadata_Toolkit"
libraries[pjmt][destination] = "libraries"
