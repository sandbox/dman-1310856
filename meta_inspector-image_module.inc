<?php
/**
 * @file Add-on support for D6 image.module and image_import.module
 *
 * These are where the first functionality was built, but are being pulled out
 * of the main project in favor of continuing better support for cck-based
 * filefield solutions.
 *
 *
 * @author [dman] Dan Morrison http://coders.co.nz/
 * @ingroup meta
 */


/**
 * Add enhancements to the image_import form.
 *
 * Form-specific hook_form_alter()
 *
 * Modify just image_import_form
 *
 * Scans the import dir for metadata from any available source.
 * Presets the title and body description fields in the form if possible.
 * Displays info about the found keyword tags.
 *
 * The data found here is mostly informational. Hidden metadata is NOT
 * transparently passed through the import process - it's re-retrieved in a
 * nodeapi callback.
 */
function meta_inspector_form_image_import_form_alter(&$form, $form_state) {
  // hook_metadata_from_dir() returns a metadata array to add extra information, possibly including a description or title.
  // The scan process has to run on both form_build and form_submit (even though the data isn't needed) just to ensure the form fields are still configured :-(
  // The scan hook is run on a whole-directory basis,
  // as some metadata formats may include one combined file for all media
  // (eg descript.ion, meta.db) and reading it all, once, is better than looking it up each time.
  $dirpath = variable_get('image_import_path', '');
  $metadata = module_invoke_all('metadata_from_dir', $dirpath);
  // Note that if there are more than one metadata source, we may get arrays where we expected strings.
  // Be sure to handle that when setting values later.

  if (!empty($form_state['post']['op']) && $form_state['post']['op'] == t('View Raw Metadata')) {
    // Utility/debug callback to view all the available metadata fields
    if (module_exists('devel')) {
      dpm($metadata);
    }
    else {
      drupal_set_message(t("<pre>%metadata</pre>", array('%metadata' => print_r($metadata, 1))));
    }
  }

  // Add more metadata support fields - Display extracted tag analysis
  if (! empty($metadata)) {
    $form['meta_inspector'] = array(
      '#type' => 'fieldset',
      '#title' => 'meta_inspector Metadata',
    );
    $form['meta_inspector']['note'] = array('#value' => t("Some metadata was discovered within the media, and will be imported if possible."));
    $all_terms = array();
    foreach ($metadata as $file_data) {
      # make a note of ALL keyword terms found
      if (isset($file_data['dc:subject'])) {
        // Merge array by values, otherwise the index number overwrites earlier ones
        $all_terms += array_combine($file_data['dc:subject'], $file_data['dc:subject']);
      }
    }
    if ($all_terms) {
      $options = $all_terms;

      $form['meta_inspector']['all_terms'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Available derived tags'),
        '#description' => t(
          "The following tags were found within the file descriptions.
          These can be used to create taxonomy terms for the images.
          <strong>Only</strong> selected ones will be used, the rest will be ignored.
          Deselect inappropriate tags.
          (Not all terms apply to all images, this is an aggregate list)"
        ),
        '#options' => $options,
        '#default_value' => $options,
        '#attributes' => array('class' => 'inline-checkboxes'),
      );
      drupal_set_html_head('<style type="text/css">.inline-checkboxes div { display:inline; } </style>');
      $vocabularies = taxonomy_get_vocabularies();
      $options = array(0 => t('<none>'));
      foreach ($vocabularies as $v) {
        $options[$v->vid] = $v->name;
      }

      $form['meta_inspector']['terms_vocabulary'] = array(
        '#type' => 'select',
        '#title' => 'Tag vocabulary',
        '#options' => $options,
        '#description' => t(
          "If you select a tag vocabulary, the terms selected above
          will be matched or inserted into it, and used to classify
          the imported media."
        ),
        '#default_value' => variable_get('meta_inspector_terms_vocabulary', 0),
      );

      $form['meta_inspector']['view_metadata'] = array(
        '#type' => 'button',
        '#value' => t('View Raw Metadata'),
      );

    }
  }
  // submit was called too late. Run the submit action at validate time instead :-(
  $form['meta_inspector']['terms_vocabulary']['#element_validate'][] = 'meta_inspector_image_import_form_submit';
}

/**
 * Modify image_node_form
 *
 * Form-specific hook_form_alter
 * Add a 'scan' button
 */
function meta_inspector_form_image_node_form_alter(&$form, $form_state) {
  // From http://drupal.org/node/368505 April 2008, image.module features are grouped in a thing we can add to

  $form['image']['rescan_meta'] = array(
    '#type' => 'submit',
    '#value' => "Scan image for metadata",
    '#submit' => array('meta_inspector_scan_image_node'),
  );

  // Capture the normal title validation so we can try to fill it in ourselves
  // Custom validation comes AFTER the #required check, (see _form_validate() ) so need to disable that
  $form['title']['#required'] = FALSE;
  $form['#validate'][] = 'meta_inspector_fill_in_values';
}

/**
 * Capture the form submission and note the preferences selected.
 *
 * image_import doesn't actually allow us to pass data through the process to
 * image_create_node_from. Instead we set some values concerning the terms, and
 * catch the image later in node_presave - where we put the metadata in.
 *
 * A form submit handler.
 */
function meta_inspector_image_import_form_submit(&$form, &$form_state) {
  // Detect selected terms and create them if needed.
  global $_meta_inspector_active_terms, $_meta_inspector_terms_vocabulary;
  $meta_inspector_active_terms = $form_state['values']['all_terms'];
  $meta_inspector_terms_vocabulary = $form_state['values']['terms_vocabulary'];
  variable_set('meta_inspector_terms_vocabulary', $meta_inspector_terms_vocabulary);
}


/**
 * Scans the given image.module image node for related metadata
 *
 * UNFINISHED
 */
function meta_inspector_scan_image_node(&$form,   $form_state) {
  drupal_set_message(t('Meta Inspector : Scanning for metadata about the file %file', array('%file' => $form_state['values']['images'][IMAGE_ORIGINAL])));

  $meta = module_invoke_all('metadata_from_file', $form_state['values']['images'][IMAGE_ORIGINAL]);
  $meta_data_array = theme('meta_data_array', $meta);
  drupal_set_message(t('!meta_data_array', array('!meta_data_array' => $meta_data_array)));

  // The form_values array is not exactly the same as a node object, but probably as near as can be expected
  $node_values = (object) $form_state['values'];
  meta_inspector_merge_metadata_with_node($node_values, $meta, TRUE);

  return "done";
  return drupal_get_form($form_state['values']['form_id'], $node_values);

  // Currently brutally save this node immediately.
  // Really want to merge the updated data into the form so it can be previewed.
  #node_save($node_values);
  $form_state['values'] = (array) $node_values;
  // Need to get the new node data to load into the form_state

  $form = form_builder($form_state['values']['form_id'], $form, $form_state);
  #dpm($form);
}
