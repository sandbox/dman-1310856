<?php
// $ID:  $
/**
 * @file
 *   Metadata for Drupal Media
 *
 * @author 'dman' Dan Morrison http://coders.co.nz/
 */

/**
 * @defgroup meta Metadata for Drupal Media
 * @{
 * This package accesses metadata and descriptions associated with images (or
 * other media files).
 *
 * The central module - meta_inspector.module - invokes other metadata
 * extraction modules ( meta_descriptionfile.module , meta_pjmt.module (includes
 * EXIF and XMP), meta_database.module ) to actually do the format- specific
 * parsing. Each of these implements hooks that return the data they find in
 * their own way.
 *
 * meta_inspector.module provides a UI to a few ways this data can be used - by
 * pro-populating title, body, or other fields into the storage node.
 *
 * You can choose any or all of the components, depending on what is supported
 * by your input
 *
 * @see index.htm, meta_inspector_help.htm, walkthrough.htm
 * @}
 */

/**
  * @defgroup drupal_hooks Implementations of Drupal hook functions
  */
