<?php
/**
 * @file A wizard form to assist configuring an image content type to handle
 * metadata that may be available to it.
 *
 * When triggered, CCK fields matching valid EXIF or XMP data tags may be added
 * to a specified content type.
 * This provides an interface for metadata field mapping and auto-creation of
 * CCK configs.
 *
 * @author [dman] Dan Morrison http://coders.co.nz/
 * @ingroup meta
 */

/**
 * Settings page form.
 *
 * The individual meta_* sub-modules may insert their own settings here via
 * form_alter.
 *
 * This form may be used by the sub-modules even in meta_inspector is not
 * enabled.
 * Weights are negative just to ensure the system_settings_form buttons remain
 * on the bottom.
 *
 * A FAPI form definition.
 */
function meta_settings() {
  $form = array();
  $form['meta_inspector'] = array(
    '#type' => 'fieldset',
    '#title' => 'Meta Inspector',
    '#weight' => -11,
  );

  $form['meta_inspector']['meta_inspector_read'] = array(
    '#type' => 'checkbox',
    '#title' => t('Read metadata from files into nodes when first uploaded'),
    '#default_value' => variable_get('meta_inspector_read', FALSE),
    '#description' => t('Recognised values will be read from files and merged with the node data (using the mapping rules) where possible.'),
  );
  // TODO manage module weighting

  // List all content types and fields
  // Build a form where the mappings can be seen.

  $scan_rules = meta_inspector_scan_rules();

  $types_subform = array(
    '#type' => 'fieldset',
    '#title' => t('Content types scan rules'),
    '#tree' => TRUE,
  );

  if (function_exists('content_types')) {
    $types = content_types();
  }
  else {
    $types = (array) node_get_types();
    // non-cck sites need love too. Just need to flatten the type defs from object to array.
    $types = array_map(create_function('$n', 'return (array)$n;'), $types);
  }
  #dpm($types);
  $rdf_mapping = module_invoke_all('rdf_mapping');
  #dpm($rdf_mapping);

  foreach ($types as $type_id => $type_def) {
    $type_def = (array) $type_def;
    $type_subform = array(
      '#type' => 'fieldset',
      '#title' => $type_def['name'],
    );
    $filefields = array();
    $has_filefields = FALSE;


    if (!empty($type_def['fields'])) {
      // Find any fields we can use. Note the settings
      foreach ($type_def['fields'] as $field_id => $field_def) {
        if ($field_def['type'] == 'filefield') {
          $has_filefields = TRUE;
          $filefields[$field_id] = $field_def['widget']['label'];
        }
      } // each field


      $type_subform['filefields'] = array(
        '#title' => t('Scan for metadata on upload'),
        '#type' => 'checkboxes',
        '#options' => $filefields,
        '#default_value' => $scan_rules[$type_id]['filefields'],
      );
      $type_subform['populate'] = array(
        '#title' => t('Populate node data from'),
        '#type' => 'select',
        '#options' => array('' => '<none>') + $filefields,
        '#default_value' => $scan_rules[$type_id]['populate'],
        '#description' => t('use this field to fill in node data (like title & body) if possible.'),
      );

    } // has fields
    if (! $has_filefields) {
      $type_subform['info'] = array(
        '#type' => 'markup',
        '#value' => t('No filefields found'),
      );
    }
    // also do file upload attachments?

    $types_subform[$type_id] = $type_subform;
  }
  $form['meta_inspector_scan_rules'] = $types_subform;

  $form['mapping_table'] = array(
    '#type' => 'markup',
    '#value' => meta_inspector_render_mapping_rules,
  );

  return system_settings_form($form);
}


/**
 * Diagnostic dump of all mapping rules.
 * Enter description here ...
 */
function meta_inspector_render_mapping_rules() {
  // Render the current mapping rules
  $content_types = content_types();
  $all_mappings = array();
  $all_fields = array();
  $header = array('source' => 'source');
  foreach ($content_types as $content_type_id => $content_type_info) {
    $header[$content_type_id] = $content_type_info['name'];
    $all_mappings[$content_type_id] = meta_inspector_mapping_rules($content_type_id);
    foreach ($all_mappings[$content_type_id] as $source => $target) {
      $all_fields[$source] = is_array($all_fields[$source]) ? $all_fields[$source] : array();
      $all_fields[$source] += (array) $target;
    }
  }
  // gathered a few lists, now theme it.
  $table = array();
  foreach ($all_fields as $field_id => $all_targets) {
    $row = array();
    $row[] = array('data' => $field_id);
    foreach ($all_mappings as $content_type_id => $column) {
      $row[] = isset( $column[$field_id] ) ? join(', ', (array) $column[$field_id]) : ''; // May be empty, that's OK
    }
    $table[$field_id] = $row;
  }
  return theme('table', $header, $table);
}

/**
 * Process for deducing available metadata values from a given image.
 *
 * A FAPI form definition
 *
 * Multistep, defers most form building to sub-forms.
 */
function meta_inspector_wizard(&$form_state) {
  $form = array();
  $steps = 3;

  // Initialize multistep
  if (empty($form_state['storage']['step'])) {
    $form_state['storage']['step'] = 1;
  }
  if (empty($form_state['values'])) {
    // Just to stop too many PHP strict notices
    $form_state['values'] = array();
  }
  // Retrieve the last submitted values from storage to re-populate this form
  // Merge remembered values under submitted values so they are always available.
  if ( isset($form_state['storage']['global'])) {
    $form_state['values'] = array_merge($form_state['storage']['global'], $form_state['values']);
  }

  $form['progress_indicator'] = array(
    '#value' => t('Step %step of %steps', array('%step' => $form_state['storage']['step'], '%steps' => $steps)),
    '#type' => 'markup',
    '#prefix' => '<h3>',
    '#suffix' => '</h3>',
  );

  // I could now switch ($form_state['storage']['step'])
  // but it's even more abstract to just go:
  $subform_name = 'meta_inspector_wizard_step_' . $form_state['storage']['step'];
  if (function_exists($subform_name)) {
    $subform = $subform_name($form_state);
    $form = array_merge($form, $subform);
  }

  // Show step buttons.
  if ($form_state['storage']['step'] > 1) {
    $form['previous'] = array(
      '#type' => 'submit',
      '#value' => t('<< Previous'),
      '#submit' => array('meta_inspector_previous_button_submit'),
    );
  }
  if ($form_state['storage']['step'] < $steps) {
    $form['next'] = array(
      '#type' => 'submit',
      '#value' => t('Next >>'),
      '#submit' => array('meta_inspector_next_button_submit'),
    );
  }
  return $form;

}


/**
 * Button press handler function.
 *
 * Stand-alone way of working with a multistep form.
 */
function meta_inspector_next_button_submit($form, &$form_state) {
  return meta_inspector_nav_button_submit($form, $form_state, 1);
}

/**
 * Button press handler function.
 *
 * Stand-alone way of working with a multistep form.
 */
function meta_inspector_previous_button_submit($form, &$form_state) {
  return meta_inspector_nav_button_submit($form, $form_state, -1);
}

/**
 * Save the values submitted on this form.
 * Move the step forwards or back.
 * Run any other (global) submission actions.
 *
 * Drupal form handler
 */
function meta_inspector_nav_button_submit($form, &$form_state, $direction = 1) {
  // Multistep: save the values for the current submitted step into the storage array
  #$form_state['storage']['values'][$form_state['storage']['step']] = $form_state['values'];
  $form_state['storage']['global'] = @array_merge((array) $form_state['storage']['global'], $form_state['values']);
  $form_state['storage']['step'] = $form_state['storage']['step'] + $direction;
//  Multistep: Tell Drupal we are redrawing the same form
  $form_state['rebuild'] = TRUE;

  // do other things the form may have been expecting to happen
  foreach ((array) @$form['#submit'] as $submit_func) {
    $submit_func($form, $form_state);
  }
}


/**
 * Subform FAPI definition
 *
 * Sets the target content type (eg image) and uploads or chooses a sample image
 * for analysis
 *
 * @param $form_state
 * @return form definition (to be merged with a larger containing form)
 */
function meta_inspector_wizard_step_1(&$form_state) {
  $form = array();

  $form['title'] = array(
    '#value' => t('Wizard for attaching metadata to node types'),
    '#type' => 'markup',
    '#prefix' => '<h2>',
    '#suffix' => '</h2>',
  );

  $form['help'] = array(
    '#type' => 'markup',
    '#value' => file_get_contents(drupal_get_path('module', 'meta_inspector') . '/help/meta_inspector_help.htm'),
  );

  $types = node_get_types();
  // We are probably working with a content type called image, even if using the CCK method
  // If not, choose your own.
  if (isset($types['image'])) {
    $form_state['values']['content_type'] = 'image';
  }

  $form['content_type'] = array(
    '#type' => 'radios',
    '#title' => t('Content type'),
    '#default_value' => isset($form_state['values']['content_type']) ? $form_state['values']['content_type'] : variable_get('meta_inspector_sample_content_type', ''),
    '#options' => array_map('check_plain', node_get_types('names')),
    '#description' => t('Select the Content type to configure with metadata support.'),
    '#required' => TRUE,
  );

  // SAMPLE IMAGE
  $form['sample_image'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sample Image'),
    '#description' => t('
      Choose or upload an image to be scanned for metadata.
      The available info tags may be used as data sources.
      <em>Different images may have different data available</em>
      depending on how they are produced.
      for this reason you may want to experiment with several samples.
    '),
  );
  if (! empty($form_state['values']['sample_image_path'])) {
    $form['sample_image']['sample_image_preview'] = array(
      '#type' => 'markup',
      '#value' => theme('image', $form_state['values']['sample_image_path'], NULL, NULL, array('width' => 100, 'height' => 100), FALSE),
    );
  }
  $form['sample_image']['sample_image_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to sample image'),
    '#default_value' => isset($form_state['values']['sample_image_path']) ? $form_state['values']['sample_image_path'] : variable_get('meta_inspector_sample_image_path', ''),
    '#element_validate' => array('meta_inspector_wizard_sample_image_path_validate'),
  );
  $form['sample_image']['sample_image_upload'] = array(
    '#type' => 'file',
    '#title' => t('Upload sample image'),
    '#element_validate' => array('meta_inspector_wizard_sample_image_upload'),
  );
  $form['#attributes'] = array('enctype' => 'multipart/form-data');

  return $form;
}

/**
 * Check the image path is valid
 *
 * A form element_validate callback.
 *
 * @see meta_inspector_wizard_step_1()
 */
function meta_inspector_wizard_sample_image_path_validate($element, &$form_state) {
  if (empty($element['#value'])) {
    return;
  }
  if (! file_check_path($element['#value'])) {
    form_set_error($element['#name'], t('%path is an invalid image path', array('%path' => $element['#value'])));
  }
}

/**
 * Uploading the image has to happen in the validate phase so that the
 * data (the new path) gets saved later.
 *
 * A form element_validate callback.
 *
 * @see meta_inspector_wizard_step_1()
 */
function meta_inspector_wizard_sample_image_upload($element, &$form_state) {
  if ($file = file_save_upload('sample_image_upload', array('file_validate_is_image' => array()))) {
    $parts = pathinfo($file->filename);
    $filename = 'meta_inspector_sample.' . $parts['extension'];
    if (file_copy($file, $filename)) {
      drupal_set_message(t('Saved image as %filename', array('%filename' => $filename)));
      $_POST['sample_image_path'] = $form_state['values']['sample_image_path'] = $file->filepath;
      form_set_value($element, $file->filepath, $form_state);
      variable_set('meta_inspector_sample_image_path', $file->filepath);
    }
    else {
      form_set_error($element, t('Failed to copy up the sample image'));
    }
  }
}

/**
 * Form displaying available metadata values retrieved from the submitted image.
 *
 * @param $form_state
 * @return form definition (to be merged with a larger containing form)
 */
function meta_inspector_wizard_step_2(&$form_state) {
  $form = array();

  $form['title'] = array(
    '#value' => t('Map metadata fields to CCK fields'),
    '#type' => 'markup',
    '#prefix' => '<h2>',
    '#suffix' => '</h2>',
  );

  $content_type_id = $form_state['values']['content_type'];
  variable_set('meta_inspector_sample_content_type', $content_type_id);

  // Remember this for convenience
  if (!empty($form_state['values']['sample_image_path'])) {
    variable_set('meta_inspector_sample_image_path', $form_state['values']['sample_image_path']);
  }

  // $content_type = node_get_types('type', $content_type_name);
  // CCK only...
  $content_type = content_types($content_type_id);

  $mapping_rules = meta_inspector_mapping_rules($content_type_id);

  // Can also map some metadata fields against vocabularies
  $vocabularies = taxonomy_get_vocabularies();
  $vocabulary_options = array();
  foreach ($vocabularies as $vocabulary) {
    $vocabulary_options['vocabulary:' . $vocabulary->vid] = 'vocabulary:' . $vocabulary->name;
  }

  // The content type definition may include CCK fields and non-cck 'extra' fields
  $available_fields = array_merge(
    array('' => ''),
    array_combine(array_keys($content_type['extra']), array_keys($content_type['extra'])),
    array_combine(array_keys($content_type['fields']), array_keys($content_type['fields'])),
    $vocabulary_options
  );
  $available_field_options = $available_fields;

  // This mapping widget prototype is copied into every row.
  $mapping_widget = array(
    'metadata_source' => array(
      '#type' => 'hidden',
    ),
    'mapped_field' => array(
      '#type' => 'select',
      '#options' => $available_field_options,
      '#default_value' => '',
    ),
    'new_field' => array(
      '#type' => 'checkbox',
      '#title' => t('New field'),
    ),
  );

  $filepath = $form_state['values']['sample_image_path'];
  if (! empty($filepath)) {
    $meta = module_invoke_all('metadata_from_file', $filepath);
  }
  else {
    $meta = array(
      'dc:title' => array('item title'),
      'dc:description' => array('descriptive text'),
    );
  }

  $form['sample_image']['sample_image_preview'] = array(
    '#type' => 'markup',
    '#value' => theme('image', $form_state['values']['sample_image_path'], NULL, NULL, array('width' => 100, 'height' => 100), FALSE),
  );
  $form['instructions'] = array(
    '#type' => 'markup',
    '#value' => t('
      Look at the available field values on the left,
      and match anything you want to keep up with a text storage field on the right.
      Other CCK field types (date, location) may always not work as expected.
      <br/>
      Many of the values found may be redundant, but you can choose the ones you want.
    '),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );

  $table = array(
    '#tree' => TRUE,
  );
  // Render a row for every possible field
  foreach ($meta as $meta_field_name => $meta_field_value) {
    $row = array();
    // #values are an ARRAY - possibly multiple values from different extraction sources
    $row['source']['#value'] = $meta_field_name . ' <br/><small><em>' . join((array) $meta_field_value, ', ') . '</em></small>';
    if (!empty($mapping_rules[$meta_field_name])) {
      $row['source']['#prefix'] = '<strong>';
      $row['source']['#suffix'] = '</strong>';
    }
    $row['destination'] = $mapping_widget;
    // Fill in the existing mapping rules
    if (isset($mapping_rules[$meta_field_name])) {
      $row['destination']['mapped_field']['#default_value'] = $mapping_rules[$meta_field_name];
    }
    $table[$meta_field_name] = $row;
  }

  $form['mapping_table'] = $table;
  $form['mapping_table']['#theme'] = 'meta_inspector_mapping_table';

  $form['#submit'][] = 'meta_inspector_wizard_step_2_submit';

  return $form;
}

/**
 * Lay out the form elements in a table.
 *
 * A form_render callback
 *
 * @param $form
 * @return HTML
 * @ingroup themable
 */
function theme_meta_inspector_mapping_table($form) {
  $headers = array(
    /*'source'*/
    0 => t('All data types that could be found in the sample image.'),
    'destination' => t('Fields in the content type definition where that data could be stored'),
  );
  // need to render all the form elements found in this table into the cells, then render the table.
  $formtable = array();
  foreach (element_children($form) as $row_name) {
    $form_row = $form[$row_name];
    foreach (element_children($form_row) as $col_name) {
      $form_cell = $form_row[$col_name];
      $formtable[$row_name][$col_name] = drupal_render($form_cell);
    }
  }
  return theme('table', $headers, $formtable);
}

/**
 * Save the mappings. Create fields if needed
 *
 * A form_submit handler
 */
function meta_inspector_wizard_step_2_submit($form, &$form_state) {
  $content_type = $form_state['storage']['global']['content_type'];

  // Set the mappings, careful not to destroy ones that were not listed on the previous screen
  $mapping_rules = meta_inspector_mapping_rules($content_type);
  foreach ($form_state['values']['mapping_table'] as $field_name => $field_row) {
    if ($field_row['destination']['mapped_field']) {
      $mapping_rules[$field_name] = $field_row['destination']['mapped_field'];
    }
    // Now create a brand new cck field if we must. Another loop, as the
    if ($field_row['destination']['new_field']) {
      $new_field = meta_inspector_wizard_create_field($field_name, $content_type);
      if ($new_field) {
        drupal_set_message(t("Created a new field on !content_type type nodes called %label (%field_name) to store metadata in.",
          array(
          '!content_type' => l($content_type, "admin/content/node-type/{$content_type}/fields"),
          '%label' => $new_field['widget']['label'],
          '%field_name' => $new_field['field_name'],
        ))
          , 'status'
        );
        $mapping_rules[$field_name] = $new_field['field_name'];
      }

    }
  }
  meta_inspector_set_mapping_rules($content_type, $mapping_rules);
  variable_set('meta_mapping_rules', $mapping_rules);

  drupal_set_message(t("You should now be able to edit the !content_type content type to adjust the fields, or try creating one and see the data get filled in.",
    array(
    '!content_type' => l($content_type, "admin/content/node-type/{$content_type}/fields"),
  ))
    , 'status'
  );

}

function meta_inspector_wizard_step_3(&$form_state) {
  $form = array();
  $form['mapping_table'] = array(
    '#type' => 'markup',
    '#value' => meta_inspector_render_mapping_rules(),
  );
  return $form;
}

/**
 * Use CCK CRUD to make fields
 * Fun!
 *
 * @return bool success
 */
function meta_inspector_wizard_create_field($label, $type_name, $field_type = 'text') {
  module_load_include('inc', 'content', 'includes/content.crud');
  $field_name = 'field_' . preg_replace('/[^a-z0-9]+/', '_', strtolower($label));
  $field = array(
    'label' => $label,
    'field_name' => $field_name,
    'type_name' => $type_name,
    'type' => 'text',
    'widget_type' => 'text_textfield',
  );
  return content_field_instance_create($field);
}
