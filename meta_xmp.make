api = 2
core = 6.x

; Libraries
libraries[arc2][download][type] = "get"
; Issues with SSL certificates mean https can be a problem.
; Use this alternate download location instead, it seems to skip the issue.
; @see http://drupal.org/node/970160
;libraries[arc2][download][url] = "https://github.com/semsol/arc2/tarball/master"
libraries[arc2][download][url] = "http://nodeload.github.com/semsol/arc2/tarball/master"

libraries[arc2][directory_name] = "ARC2/arc"
libraries[arc2][destination] = "libraries"
