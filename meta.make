api = 2
core = 6.x

; Recursive drush make downloads only look for the name of the project, not of its
; modules inside.
; So, name our internal per-module makefiles here

includes[meta_pjmt] = "meta_pjmt.make"
includes[meta_xmp] = "meta_xmp.make"

