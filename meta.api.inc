<?php
/**
 * @file documentation for the meta_ hooks
 *
 * @author [dman] Dan Morrison http://coders.co.nz/
 * @ingroup meta
 */

/**
 *
 * A mechanism for modules to extract and return information (metadata or
 * content) about given files, eg media images.
 *
 * WHY READ - to assist adding captions to images when importing them in bulk
 * from other places ... like a desktop image management tool.
 *
 * WHY WRITE - so that files/image directories can be accessed using other
 * means, or exported/archived to disk with at least some of the extra metadata
 * retained.
 *
 * WHY? - Because flat-file storage of information about files is still one of
 * the most robust and portable ways of keeping information about them available
 * when archiving, zipping, emailing, or transferring them. Databases are more
 * powerful for real classifications, but you are always locked into using the
 * database to get that information out again. Flat-file information from 10
 * years ago is still available if you dig up the old disks. Not so with any
 * CMS or software package.
 *
 * This type of functionality (the hook_describemedia()) can be re-implemented
 * in other modules to read, for example:
 *
 *  =  metadata.xml as found in some fileshare apps,
 *  =  playlist.m3u MP3 Playlist information
 *  =  Embedded EXIF data or ID3 tags
 *  =  RSS media or podcast formats
 *  =  xmlsitemap information about files or URLs
 *
 * Arrays are used for all metadata attribute values, to defer conflicts and
 * over- writes, and also to support import to multiple-valued CCK fields etc.
 *
 * Attribute names are namespace qualified if appropriate (see the XMP spec for
 * examples)
 *
 ***/


/**
 * Return metadata relating to a given filepath
 *
 * The $file object passed in is annotated by reference.
 *
 * The $file object must contain a $file->filename value.
 *
 * The object may contain $file->title, $file->body values.
 *
 * If not already set, and valid values are found, the title and body may be set
 * by this function.
 *
 * The $file object may contain a $file->metadata array. Within that array are a
 * collection of keys (attribute names) each of which has an ARRAY of values
 * underneath it. Even single-valued attributes are stored in the metadata array
 * as a n array with one element. This is for consistency in accessing them
 * again later, and to avoid premature logic when deciding which of the valid
 * value to use.
 *
 * The attribute value array MAY optionally use unique keys to indicate the
 * provenance of the data, but this is only for auditing.
 *
 * Simple example.
 *
 *   $file = stdClass (
 *     filename => 'sites/default/files/images',
 *   );
 *
 *  HOOK_file_metadata($file);
 *  Result:
 *
 *   $file = stdClass (
 *     filename => 'sites/default/files/images',
 *     body     => 'Screenshot of the image_import screen before additions.',
 *     title    => 'image_import basic screen',
 *   );
 *
 *
 *
 *
 */
function HOOK_file_metadata(&$file) {
  // This is a stub example function.

  // Only set the body if it's not already preset.
  if (empty($file->body)) {
    $file->body = 'This is a picture';
  }

  // Add namespaced values to the metadata array.
  // As elements in a nested array - for safe merging
  $file->metadata['dc:description'][] = 'This is a picture';
  $file->metadata['dc:creator'][] = 'Leonardo da Vinci';
  $file->metadata['dc:subject'][] = 'art';
  $file->metadata['dc:subject'][] = 'classics';
  $file->metadata['dc:subject'][] = 'fresco';

  // Auto-increment arrays as above are fine,
  // but to assist tracing the data sources,
  // add an identifier when adding to the array.
  $file->metadata['dc:title']['mymodule'] = 'The Mona Lisa';
  //
  // this means that later we can resolve why the metadata may have ended up with several valid values
  // $file->metadata['dc:title'] = array(
//   'mymodule' => 'The Mona Lisa',
//   'EXIF' => 'mona_lisa',
//   'usercaption' = 'Mona lisa!'
  // );
  // and possibly resolve the conflict.

}


/**
 *
 *
 * @param $dirpath a folder to scan, recursively.
 * - may also be a single file getting information requested about it
 *
 * @return $metadata an array, keyed by filename, that may contain attributes,
 * such as title, caption, or other relevant values.
 *
 */
function HOOK_metadata_from_dir($dir) {
  // This is a stub example function.
  return array(
    $dir => array('dc:description' => 'description of this dir'),
    $dir . '/a_file.jpg' => array('dc:description' => 'description of this file'),
  );
}

